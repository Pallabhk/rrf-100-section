jQuery(document).ready(function($){

	$(".portfolio-filter li").on('click',function(){
		
		var filterData = $(this).attr("data-filter");

		$(".portfolio-list").isotope({
			filter:filterData,



		});
		$(".portfolio-filter li").removeClass('active');
		$(this).addClass('active');
	});
	$(".portfolio-list").isotope({

		// set itemSelector so .grid-sizer is not used in layout
		  itemSelector: '.single-portfolio-item',
		  percentPosition: true,
		  masonry: {
		    // use element for option
		    columnWidth: '.single-portfolio-item',
		     horizontalOrder: true,
 			 }
	});

	$("body").perfectScrollbar();
	$(".scrooled-another").perfectScrollbar({
		theme:'yellow-theme'
	});
});