jQuery(document).ready(function($){

	$(".portfolio-list").masonry({
		
	});

	$(".single-portfolio-item").hover(function(){
		$(".single-portfolio-item .porfolio-hover h2").removeClass("slideInDown")
		$(this).find(".porfolio-hover h2").addClass("animated slideInDown");
	});
	$(".menu-trigger").on('click', function(){
		$(".coffcanvs-menu").addClass("active");
		$(".offcanvas-menu-overlay").addClass("active");
	});
	$(".menu-close i.fa").on('click', function(){
		$(".coffcanvs-menu").removeClass("active");
		$(".offcanvas-menu-overlay").removeClass("active");
	});

	$(".header-area").headroom();
	
});