jQuery(document).ready(function($){
	$(".wellcome-area").YTPlayer({
		 fitToBackground: true,
         videoId: 'LSmgKRx5pBo'
	});
	var homepageSlides =$(".homepage-slides");
	homepageSlides.owlCarousel({
		items: 1,
		autoplay:false,
		loop:true,
		dots:true,
		nav:true,
		navText: ["<i class='fa fa-long-arrow-left'></i>" , "<i class='fa fa-long-arrow-right'></i>"]
	});
	homepageSlides.on('translate.owl.carousel',function(event){
		$(".wellcome-area-text h4").removeClass("animated bounceIn");
		$(".wellcome-area-text h1").removeClass("animated bounceInRight");
		$(".wellcome-area-text h1").removeClass("animated fadeIn");
	});
	
	homepageSlides.on('translated.owl.carousel',function(event){
		$(".wellcome-area-text h4").addClass("animated bounceIn");
		$(".wellcome-area-text h1").addClass("animated bounceInRight");
		$(".wellcome-area-text h1").addClass("animated fadeIn");
	});

});